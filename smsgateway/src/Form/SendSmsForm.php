<?php

/**
 * @file
 * Contains \Drupal\smsgateway\Form\SendSmsForm.
 */

namespace Drupal\smsgateway\Form;

use Drupal\Core\Form\FormBase;
use Drupal\user\Entity\User;

/**
 * Provides sending SMS form.
 */
class SendSmsForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'send_sms_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    // Build form fields structure.

    // Phone number field.
    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#description' => $this->t('The phone number that will receive SMS message.'),
      '#default_value' => '',
      '#required' => TRUE,
    ];

    // Message field.
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Body'),
      '#description' => $this->t('The message body that will send.'),
      '#default_value' => '',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send SMS'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $phone = $form_state->getValue('phone_number');
    $msg = $form_state->getValue('message');

    // validate phone number value.
    $valid_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
    $valid_number = str_replace("-", "", $valid_number);

    if (strlen($valid_number) < 10 || strlen($valid_number) > 14) {
      $form_state->setErrorByName('phone_number', $this->t('Invalid phone number.'));
    }
    // Validate message body
    if (strlen($msg) < 3) {
      $form_state->setErrorByName('message', $this->t('Invalid message.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $user = User::load($this->currentUser()->id());
    $number = $form_state->getValue('phone_number');
    $message = $form_state->getValue('message');

    $sms = (new \Drupal\sms\Message\SmsMessage())
      ->setMessage($message) // Set the message.
      ->addRecipient($number) // Set recipient phone number
      ->setDirection(\Drupal\sms\Direction::OUTGOING);

    /** @var \Drupal\sms\Provider\SmsProviderInterface $sms_service */
    $sms_service = \Drupal::service('sms.provider');

    try {
      $sms_service->queue($sms);
      $this->messenger()->addMessage($this->t('Message has been sent.'));
    } catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Message could not be sent: @error', [
        '@error' => $e->getMessage(),
      ]), 'error');
    }
  }
}
