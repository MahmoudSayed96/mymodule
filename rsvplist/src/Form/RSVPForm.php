<?php

/**
 * @file
 * Contains \Drupal\rsvplist\Form\RSVPForm.php
 */

namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;

/**
 * Provides RSVP email form
 */
class RSVPForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'rsvplist_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      // You can get nid
      $nid = $node->id();
    } else $nid = null;
    // Build form fields
    $form['email'] = array(
      '#type' => 'textfield',
      '#name' => 'email',
      '#size' => 25,
      '#title' => $this->t('Email Address'),
      '#description' => $this->t('we\'ll send updates to the email address you provide.'),
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Send RSVP')
    );

    $form['nid'] = array(
      '#type' => 'hidden',
      '#value' => $nid
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $email = $form_state->getValue('email');
    if ($email == !\Drupal::service('email.validator')->isValid($email)) {
      $form_state->setErrorByName('email', $this->t('Incorrect email, Please enter correct email'));
      return;
    }
    $node = \Drupal::routeMatch()->getParameter('node');
    // Check if email already is set for this node
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', array('nid'));
    $select->condition('nid', $node->id());
    $select->condition('mail', $email);
    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      // We found a row with this nid and email.
      $form_state->setErrorByName('email', $this->t('The address @mail is already subscribed to this list.', array('@mail' => $email)));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $conn =  \Drupal\Core\Database\Database::getConnection();
    // Insert data into database table
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $result = $conn->insert('rsvplist')
      ->fields([
        'mail' => $form_state->getValue('email'),
        'nid' => $form_state->getValue('nid'),
        'uid' => $user->id(),
        'created' => time(),
      ])->execute();
    $this->messenger()->addMessage($this->t('Thank You'));
  }
}
