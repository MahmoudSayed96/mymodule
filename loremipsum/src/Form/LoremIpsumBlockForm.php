<?php

/**
 * @file
 * Contains Drupal\loremipsum\Form\LoremIpsumBlockForm.php
 */

namespace Drupal\loremipsum\Form;

use Drupal\Core\Form\FormBase;

/**
 * Lorem ipsum block form
 */
class LoremIpsumBlockForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'loremipsum_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    // How many paragraphs?
    // $options = new array();
    $options = array_combine(range(1, 10), range(1, 10));
    $form['paragraphs'] = array(
      '#type' => 'select',
      '#title' => $this->t('Paragraphs'),
      '#options' => $options,
      '#description' => $this->t('How many?'),
      '#default_value' => 4,
    );

    // How many phrases?
    $form['phrases'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phrases'),
      '#description' => $this->t('How many?'),
      '#default_value' => '20',
    ];

    // Submit Btn
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    // make some form inputs fields validations

    // get phrases value
    $phrases = $form_state->getValue('phrases');
    // check it is a number value
    if (!is_numeric($phrases)) {
      $form_state->setErrorByName('phrases', $this->t('Please use number.'));
    }
    // check it is a integer value
    if (floor($phrases) != $phrases) {
      $form_state->setErrorByName('phrases', $this->t('Please use integer value.'));
    }
    // check it isn't a negative value
    if ($phrases < 1) {
      $form_state->setErrorByName('phrases', $this->t('Please use a number greater than zero.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $form_state->setRedirect('loremipsum.generate', [
      'paragraphs' => $form_state->getValue('paragraphs'),
      'phrases' => $form_state->getValue('phrases'),
    ]);
  }
}
