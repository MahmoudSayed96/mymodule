<?php

/**
 * @file
 * Contains Drupal\loremipsum\Plugin\Block\LoremIpsumBlock.php
 */

namespace Drupal\loremipsum\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Provides a Lorem ipsum block with which you can generate dummy text anywhere.
 *
 * @Block(
 *    id = "loremipsum_block",
 *    admin_label = @Translation("Lorem ipsum block"),
 *    category = @Translation("Custom"),
 * )
 */
class LoremIpsumBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    // Return the form @ Form/LoremIpsumBlockForm.php.
    return \Drupal::formBuilder()->getForm('Drupal\loremipsum\Form\LoremIpsumBlockForm');
  }

  /**
   * {@inheritdoc}
   */
  // public function blockAccess(AccessInterface $account)
  // {
  //   return AccessResult::allowedIfHasPermission($account, 'generate lorem ipsum');
  // }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $this->setConfigurationValue('loremipsum_block_settings', $form_state->getValue('loremipsum_block_settings'));
  }
}
