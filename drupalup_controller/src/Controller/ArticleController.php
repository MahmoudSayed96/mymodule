<?php
/**
 * @file
 * Contains \Drupal\drupalup_controller\Controller\ArticleController.
 */
namespace Drupal\drupalup_controller\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides ArticleController implementation.
 */
class ArticleController extends ControllerBase {
  /**
   * Return list of articles
   */
  public function articlesList() {
    $articles = [
      ['title' => 'Article One','body' => 'Article One body'],
      ['title' => 'Article Two','body' => 'Article Two body'],
      ['title' => 'Article Three','body' => 'Article Three body'],
      ['title' => 'Article Four','body' => 'Article Four body'],
      ['title' => 'Article Five','body' => 'Article Five body'],
    ];
    return [
      '#theme' => 'articles_list',
      '#items' => $articles,
      '#title' => $this->t('Our Articles List'),
    ];
  }
}
