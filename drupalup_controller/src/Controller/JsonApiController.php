<?php
/**
 * @file
 * Contains \Drupal\drupalup_controller\Controller\JsonApiController.
 */
namespace Drupal\drupalup_controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Json Api Controller class.
 */
class JsonApiController {
  /**
   *
   */
  public function renderApi() {
    return new JsonResponse([
      'data' => $this->getResponse(),
      'method' => 'GET'
    ]);
  }

  /**
   *  Return json response
   */
  private function getResponse() {
    return [
      [
      "id"=> 1,
      "username"=> "rbeekman0",
      "email"=> "akilby0@so-net.ne.jp",
      "address"=> "2008 Sheridan Park"
    ],
    [
      "id" => 2,
      "username" => "fwadesworth1",
      "email" => "wdahlback1@mapquest.com",
      "address" => "204 Daystar Avenue"
    ],
    [
      "id" => 3,
      "username" => "gzumfelde2",
      "email" => "haxston2@umich.edu",
      "address" => "49851 Hintze Lane"
    ],
    [
      "id" => 4,
      "username" => "tdungate3",
      "email" => "dbentjens3@printfriendly.com",
      "address" => "54 Toban Center"
    ],
    [
      "id" => 5,
      "username" => "fbowes4",
      "email" => "ntabb4@cbc.ca",
      "address" => "94971 Carpenter Road"
    ],
    [
      "id" => 6,
      "username" => "sgavigan5",
      "email" => "bstear5@myspace.com",
      "address" => "49021 Eagan Street"
    ],
    [
      "id" => 7,
      "username" => "givermee6",
      "email" => "jbassingham6@mysql.com",
      "address" => "6 Golden Leaf Place"
    ],
    [
      "id" => 8,
      "username" => "lcotterill7",
      "email" => "rjeschner7@shinystat.com",
      "address" => "086 Hanover Avenue"
    ],
    [
      "id" => 9,
      "username" => "glimpricht8",
      "email" => "cdallosso8@digg.com",
      "address" => "16 Fisk Center"
    ],
    [
      "id" => 10,
      "username" => "mdarby9",
      "email" => "jpatise9@squidoo.com",
      "address" => "803 Dryden Junction"
    ]
    ];
  }
}
