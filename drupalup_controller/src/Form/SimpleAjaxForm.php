<?php
/**
 * @file
 * Contains \Drupal\drupalup_controller\Form\SimpleForm.
 */
namespace Drupal\drupalup_controller\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides SimpleForm implementation.
 */
class SimpleAjaxForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupalup_simple_ajax_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_msg"></div>'
    ];
    $form['num1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number 1')
    ];
    $form['num2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number 2')
    ];
    $form['actions']['ajax'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate With Ajax'),
      '#ajax' => [
        'callback' => '::calculateAjax'
      ]
    ];
    $form['actions']['js'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate With JS'),
      '#ajax' => [
        'callback' => '::calculateJs'
      ]
    ];
    // include javascript file
    $form['#attached']['library'][] = 'drupalup_controller/scripts';
    return $form;
  }

  /**
   * Return ajax response with result.
   */
  public function calculateAjax(array &$form, FormStateInterface $form_state) {
    $response  = new AjaxResponse();
    $result = $form_state->getValue('num1') + $form_state->getValue('num2');
    $response->addCommand(new HtmlCommand(
      '.result_msg',
      '<div class="my_top_message">'.
        $this->t('The result is @result',['@result' => $result])
      .'</div>'));
      return $response;
  }

  public function calculateJs(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $num1 = $form_state->getValue('num1');
    $num2 = $form_state->getValue('num2');
    $response->addCommand(
      new InvokeCommand(NULL,'calculate',[$num1,$num2])
    );
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
  }
}
