<?php
/**
 * @file
 * Contains \Drupal\drupalup_controller\Form\SimpleForm.
 */
namespace Drupal\drupalup_controller\Form;

use Drupal\Core\Form\FormBase;

/**
 * Provides SimpleForm implementation.
 */
class SimpleForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupalup_simple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $form['num1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number 1')
    ];
    $form['num2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number 2')
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    $this->messenger()->addMessage($form_state->getValue('num1') + $form_state->getValue('num2'));
  }
}
