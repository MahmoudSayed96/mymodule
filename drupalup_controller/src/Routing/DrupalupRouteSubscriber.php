<?php
/**
 * @file
 * Contains \Drupal\drupalup_controller\Routing\DrupalupRouteSubscriber.
 */

namespace Drupal\drupalup_controller\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;

/**
 * Listen to dynamic route events and enable us to alter theme.
 */
class DrupalupRouteSubscriber extends RouteSubscriberBase
{
    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(\Symfony\Component\Routing\RouteCollection $collection)
    {
        // Alter user routes
    // $user_login_route = $collection->get('user.login');
    // $user_login_route->setPath('/hell/login');
    // $user_route = $collection->get('user.page');
    // $user_route->setPath('/hell');
    }
}
