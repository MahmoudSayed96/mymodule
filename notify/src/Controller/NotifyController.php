<?php

/**
 * @file
 * Contains Drupal\notify\Controller\NotifyController.
 */

namespace Drupal\notify\Controller;

use Drupal\Core\Controller\ControllerBase;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implement NotifyController actions.
 */
class NotifyController extends ControllerBase
{
    const SERVER_API_KEY = 'AAAA5eTVq3Q:APA91bH_bnoRVTNXzVNtHLFIQgUjAfJGcBlwI20EZP3NMg0TSZVdlwh_Ram__qz6S3V6ko7Bg5kdyxnGADSIbx_N6lFDzjad-GHgMdGZZDBzUyAx_-UKQ0Mp9FaAkdBrBHgcI5UmySn8';
    /**
     * Save token into database table.
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @return Symfony\Component\HttpFoundation\JsonResponse $response
     */
    public function addToken(Request $request)
    {
        $postReq = $request->request->all();
        $token = $postReq['token'];
        $currentUser = \Drupal::currentUser();
        // DB connection
        $connection =  \Drupal\Core\Database\Database::getConnection();
        $result = $connection->insert('tokens')
          ->fields([
            'id'          => null,
            'token'       =>$token,
            'uid'         => $currentUser->id(),
            'created_at'  => \Drupal::time()->getRequestTime(),
          ])->execute();
        if ($result) {
            return new JsonResponse([
          'message'=>'Token saved successfully',
          'result' => $result
        ]);
        }
        return new JsonResponse([
        'message'=>'Server error',
        'result' => $result
      ]);
    }

    public function send()
    {
        // DB connection
        $connection =  \Drupal\Core\Database\Database::getConnection();
        $stmt = $connection->query("SELECT * FROM tokens");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Store tokens in array
        foreach ($rows as $row) {
            $registrationIds[] = $row['token'];
        }

        $header = [
        'Authorization: key='. self::SERVER_API_KEY,
        'Content-Type: Application/json'
      ];
        $msg = [
        'title' => 'Message Title',
        'body' => 'Message Body'
      ];
        $payload = [
        'registration_ids' => $registrationIds,
        'data' => $msg
      ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($payload),
        CURLOPT_HTTPHEADER => $header
      ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return new JsonResponse([
            'message' =>"cURL error :".$err
          ]);
        } else {
            return new JsonResponse([
            'message' =>$response
          ]);
        }
    }
}
