// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.14.3/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.3/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: "AIzaSyDy4-37nljP12iWxbhFWRfwLmGLZ4-h7g8",
  authDomain: "push-notifications-a2aa2.firebaseapp.com",
  databaseURL: "https://push-notifications-a2aa2.firebaseio.com",
  projectId: "push-notifications-a2aa2",
  storageBucket: "push-notifications-a2aa2.appspot.com",
  messagingSenderId: "987386719092",
  appId: "1:987386719092:web:35e6db76817883d9351a21"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = payload.data.title;
  var notificationOptions = {
    body: payload.data.body,
    //icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle, notificationOptions);
});
