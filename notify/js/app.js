'use strict';
(function ($, Drupal, drupalSettings, navigator) {
  'use strict';
  // Firebase app config
  var firebaseConfig = {
    apiKey: "AIzaSyDy4-37nljP12iWxbhFWRfwLmGLZ4-h7g8",
    authDomain: "push-notifications-a2aa2.firebaseapp.com",
    databaseURL: "https://push-notifications-a2aa2.firebaseio.com",
    projectId: "push-notifications-a2aa2",
    storageBucket: "push-notifications-a2aa2.appspot.com",
    messagingSenderId: "987386719092",
    appId: "1:987386719092:web:35e6db76817883d9351a21"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // Retrieve Firebase Messaging object.
  var messaging = firebase.messaging();
  navigator.serviceWorker.register("/modules/custom/notify/js/firebase-messaging-sw.js").then(function (registration) {
    messaging.useServiceWorker(registration);
    // Request permission and get token.....
    // Ask user to allow notifications
    messaging.requestPermission()
      .then(function (state) {
        if (isTokenSentToServer()) {
          console.log('Token already sent to server.');
        } else {
          getRegisterToken();
        }
      }).catch(function (err) {
        console.error("Unable to subscribe to notifications.", err);
      });
  }).catch(function (err) {
    console.error('Unable to register service worker ', err);
  })
  // Send message
  messaging.onMessage(function (payload) {
    console.log(payload);
    var notificationTitle = payload.data.title;
    var notificationOptions = {
      'body': payload.data.body
    };
    var notification = new Notification(notificationTitle, notificationOptions);
  });

  // FUNCTIONS
  function getRegisterToken() {
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function (currentToken) {
      if (currentToken) {
        saveToken(currentToken);
        setTokenSentToServer(true);
        console.log(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        setTokenSentToServer(false);
      }
    }).catch(function (err) {
      console.log('An error occurred while retrieving token. ', err);
      setTokenSentToServer(false);
    });
  }

  // For don't create token more than once
  function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
  }

  // Check token sent to server
  function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
  }

  // Send token to server to save it into DB
  function saveToken(token) {
    $.ajax({
      url: "/add/token",
      method: "POST",
      data: {
        token
      },
      success: function (res) {
        console.log(res);
      }
    });
  }
})(jQuery, Drupal, drupalSettings, navigator);
