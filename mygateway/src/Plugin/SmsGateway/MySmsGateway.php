<?php

/**
 * @file
 * Contains \Drupal\mygateway\Plugin\SmsGateway\MySmsGateway.
 */

namespace Drupal\mygateway\Plugin\SmsGateway;

use Drupal\sms\Entity\SmsGatewayInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGateway\LogGateway;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Plugin\SmsGatewayPluginInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides custom sms gateway plugin
 *
 * @SmsGateway(
 *    id = "my_gateway",
 *    label = @Translation("My Gateway"),
 *    incoming = TRUE,
 *    incoming_route = TRUE,
 * )
 */
class MySmsGateway extends SmsGatewayPluginBase implements SmsGatewayPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public function processIncoming(Request $request, SmsGatewayInterface $sms_gateway)
  {
    $messages = [];

    /** @var  \Drupal\sms\Entity\SmsMessage $message */
    $message = \Drupal\sms\Entity\SmsMessage::create();

    // Create message
    $message->setDirection(\Drupal\sms\Direction::INCOMING)
      ->addRecipient($request->get('number'))
      ->setMessage($request->get('message'))
      ->setGateway($sms_gateway);

    // Generate unique id for messages
    $unique_message_id = md5(uniqid(time(), true));

    // Create result and report.
    $result = new \Drupal\sms\Message\SmsMessageResult();
    $report = (new \Drupal\sms\Message\SmsDeliveryReport())
      ->setRecipient($request->get('number'))
      ->setStatus(\Drupal\sms\Message\SmsMessageReportStatus::DELIVERED)
      ->setMessageId($unique_message_id); // An optional unique message ID.
    $result->addReport($report);
    $message->setResult($result);

    $response = new \Symfony\Component\HttpFoundation\Response('', 204);
    $task = (new \Drupal\sms\SmsProcessingResponse())
      ->setResponse($response)
      ->setMessages($messages);
    return $task;
  }
  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms)
  {
    $result = new SmsMessageResult();
    $reports = [];

    foreach ($sms->getRecipients() as $recipient) {
      // ...Send message with API
      $reports[] = (new SmsDeliveryReport())
        ->setRecipient($recipient)
        ->setStatus(SmsMessageReportStatus::DELIVERED);
    }

    $result->setReports($reports);
    return $result;
  }
}
