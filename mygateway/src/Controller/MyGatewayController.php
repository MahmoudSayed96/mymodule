<?php

/**
 * @file
 * Contains \Drupal\mygateway\Controller\MyGatewayController.
 */

namespace Drupal\mygateway\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sms\Entity\SmsGatewayInterface;
use Drupal\sms\Entity\SmsMessageInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides implements of MyGatewayController class
 */
class MyGatewayController extends ControllerBase
{
  /**
   * Provides callback Handle incoming route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\sms\Entity\SmsGatewayInterface $sms_gateway
   */
  public function handleIncomingSmsRequest(Request $request, SmsGatewayInterface $sms_gateway)
  {
    /** @var  \Drupal\sms\Entity\SmsMessage $message */
    $message = \Drupal\sms\Entity\SmsMessage::create();

    // Create a message.
    $message->setDirection(\Drupal\sms\Direction::INCOMING)
      ->setMessage($request->get('message'))
      ->addRecipients([$request->get('number')])
      ->setGateway($sms_gateway);

    // Generate unique id for messages
    $unique_message_id = md5(uniqid(time(), true));

    // Make a report result.
    $result = new \Drupal\sms\Message\SmsMessageResult();
    $report = new \Drupal\sms\Message\SmsDeliveryReport();
    $report->setRecipient($request->get('number'))
      ->setStatus(\Drupal\sms\Message\SmsMessageReportStatus::DELIVERED)
      ->setMessageId($unique_message_id);

    $result->addReport($report);
    $message->setResult($result);

    // Queue message
    /**  @var \Drupal\sms\Provider\SmsProviderInterface $sms_provider */
    $sms_provider = \Drupal::service('sms.provider');

    try {
      $sms_provider->queue($message);
    } catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Message could not be sent: @error', [
        '@error' => $e->getMessage(),
      ]), 'error');
    }
  }
}
