<?php

/**
 * @file
 * Contains \Drupal\mymodule\Form\MySettingsForm
 */

namespace Drupal\mymodule\Form;

use Drupal\Core\Form\ConfigFormBase;

/**
 * Provides admin settings form
 */
class MySettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mymodule_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // return settings file
    return [
      'mymodul.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    // Default settings
    $config = $this->config('mymodule.settings');
    // Facebook account URL
    $form['facebook_account_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Facebook account URL'),
      '#description' => $this->t('Typing an facebook account url'),
      '#default_value' => $config->get('admin.facebook_account_url'),
    );
    // Admin Phone number
    $form['phone_number'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Admin Phone number'),
      '#description' => $this->t('Typing an Admin Phone number'),
      '#default_value' => $config->get('admin.phone_number'),
    );
    // Privacy Policy Text
    $form['privacy_policy_text'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Privacy Policy Text'),
      '#description' => $this->t('Typing Privacy Policy Text'),
      '#default_value' => $config->get('admin.privacy_policy_text'),
    );

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('mymodule.settings')
      ->set('admin.facebook_account_url', $form_state->getValue('facebook_account_url'))
      ->set('admin.phone_number', $form_state->getValue('phone_number'))
      ->set('admin.privacy_policy_text', $form_state->getValue('privacy_policy_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
